import ChatList from './ChatList';
import ConnectedWidget from './ConnectedWidget';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReconnectingWebSocket from 'reconnectingwebsocket'
import './App.css';

const wsUri = "wss://forums.darklordpotter.net/hedwig/ws/";
//const wsUri = "ws://127.0.0.1:1255/ws/";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      chats: [],
      connected: false,
    };
    this.pinger = null;
  }

  componentDidMount() {
    const websocket = new ReconnectingWebSocket(wsUri);
    websocket.onopen = event => {
      this.setState({
        chats: [],
        connected: true
      });

      this.ping(websocket);
    }
    websocket.onclose = event => {
      this.setState({
        connected: false
      });
    }
    websocket.onmessage = event => {
      var new_chats = [...this.state.chats, JSON.parse(event.data)];
      if (new_chats.length > 100) {
        new_chats = new_chats.slice(new_chats.length - 100, 101);
      }

      this.setState({ chats: new_chats });
    };
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    return (
      <div className="App">
        <header>
          <ConnectedWidget connected={this.state.connected} />
        </header>
        <section ref="messageList">
          <ChatList chats={this.state.chats} />
        </section>
      </div>
    );
  }

  ping = (websocket) => {
    if (this.state.connected) {
      if (this.pinger != null) {
        clearTimeout(this.pinger);
      }
      websocket.send("ping");
      this.pinger = setTimeout(() => this.ping(websocket), 60000);
    }
  }

  scrollToBottom = () => {
      const { messageList } = this.refs;
      const scrollHeight = messageList.scrollHeight;
      const height = messageList.clientHeight;
      const maxScrollTop = scrollHeight - height;
      ReactDOM.findDOMNode(messageList).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  }
}

export default App;
