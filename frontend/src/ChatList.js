import Linkify from 'react-linkify';
import React from "react"
import renderHTML from 'react-render-html';
import Moment from 'react-moment';
import "./ChatList.css";

export default ({ chats }) => (
  <ul>
    {chats.map(chat => {
      return (
        <div>
          <div className="chatMessage">
            <div key={chat.id} className="box">
              <Event chat={chat} />
            </div>
          </div>
        </div>
      );
    })}
  </ul>
);

function Event(props) {
  const chat = props.chat;
  if (chat.event.type === 'PRIVMSG') {
    return <Message chat={chat} />;
  } else if (chat.event.type === 'ACTION') {
    return <Action chat={chat} />;
  } else if (chat.event.type === 'JOIN') {
    return <Join chat={chat} />;
  } else if (chat.event.type === 'PART') {
    return <Part chat={chat} />;
  } else if (chat.event.type === 'QUIT') {
    return <Quit chat={chat} />;
  }
}

function Message(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span class="event-message">
        &lsaquo;<b>{chat.username}</b>&rsaquo; <Linkify>{ircToHtml(chat.event.message)}</Linkify>
      </span>
    </div>
  )
}

function Action(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span class="event-action">* {chat.username} {chat.event.action}</span>
    </div>
  )
}

function Join(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span class="event-join">→ {chat.username} joined.</span>
    </div>
  )
}

function Part(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span class="event-part">← {chat.username} left.</span>
    </div>
  )
}

function Quit(props) {
  const chat = props.chat;
  return (
    <div>
      <Date>{chat.date}</Date>
      <span class="event-part">⇐ {chat.username} quit.</span>
    </div>
  )
}

function Date(props) {
  const date = props.children;
  return (
    <span class="event-date">
      <Moment format="HH:mm">{date}</Moment>
    </span>
  )
}

function ircToHtml(text) {
    //control codes
    var rex = /\003([0-9]{1,2})[,]?([0-9]{1,2})?([^\003]+)/;

    var fg = '';
    var bg = '';
    if (rex.test(text)) {
        var cp = null;
        var cbg = null;
        while (cp = rex.exec(text)) {
            if (cp[2]) {
                cbg = cp[2];
            }
            text = text.replace(cp[0],'<span class="fg'+cp[1]+' bg'+cbg+'">'+cp[3]+'</span>');
        }
    }
    //bold,italics,underline (more could be added.)
    var bui = [
        [/\002([^\002]+)(\002)?/, ["<b>","</b>"]],
        [/\037([^\037]+)(\037)?/, ["<u>","</u>"]],
        [/\035([^\035]+)(\035)?/, ["<i>","</i>"]]
    ];
    for (var i=0;i < bui.length;i++) {
        var bc = bui[i][0];
        var style = bui[i][1];
        if (bc.test(text)) {
            var bmatch = null;
            while (bmatch = bc.exec(text)) {
                text = text.replace(bmatch[0], style[0]+bmatch[1]+style[1]);
            }
        }
    }

    return (
      <span class>{renderHTML(text)}</span>
    );
}