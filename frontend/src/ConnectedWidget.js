import React from "react";

export default ({ connected }) => {
  if (connected) {
    return (<div>Connected.</div>);
  } else {
    return (<div>Disconnected.</div>);
  }
};
