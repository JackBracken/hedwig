#[macro_use]
extern crate actix;
extern crate actix_web;
extern crate backoff;
extern crate chrono;
extern crate env_logger;
extern crate irc;
#[macro_use]
extern crate log;
extern crate rand;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
#[macro_use]
extern crate structopt;

use actix::prelude::*;
use actix_web::server::HttpServer;
use actix_web::App;
use common::HedwigConfig;
use irc_actor::IrcActor;
use pubsub::MessageHandler;
use structopt::StructOpt;

mod common;
mod irc_actor;
mod pubsub;
mod web;

fn main() {
    let config: HedwigConfig = HedwigConfig::from_args();

    env_logger::init();

    let sys = actix::System::new("hedwig");
    let message_database = Arbiter::start(|_| MessageHandler::with_scrollback_capacity(100));

    let mdb = message_database.clone();
    let cfg_clone = config.clone();
    let cfg_clone2 = config.clone();

    let _irc_actor: Addr<Unsync, IrcActor> =
        actix::Supervisor::start(move |_| IrcActor::new(mdb, &cfg_clone));

    // Create Http server with websocket support
    HttpServer::new(move || {
        // Websocket sessions state
        let state = web::HedwigState {
            addr: message_database.clone(),
            config: cfg_clone2.clone(),
        };

        App::with_state(state)
            // websocket
            .resource("/ws/", |r| r.route().f(web::chat_route))
    }).bind(config.listen_addr())
        .unwrap()
        .start();

    std::process::exit(sys.run());
}
