use actix::prelude::*;
use actix::Supervised;
use backoff::backoff::Backoff;
use backoff::{ExponentialBackoff, SystemClock};
use common::{HedwigConfig, IrcPayload};
use irc::client::prelude::*;
use irc::error::IrcError;
use irc::proto::Message as IrcMessage;
use pubsub::MessageHandler;
use std::time::{Duration, Instant};

pub struct IrcActor {
    addr: Addr<Syn, MessageHandler>,
    backoff: ExponentialBackoff,
    config: Config,
}

impl IrcActor {
    pub fn new(addr: Addr<Syn, MessageHandler>, config: &HedwigConfig) -> IrcActor {
        let config = Config {
            nickname: Some(config.nickname().to_string()),
            server: Some(config.server().to_string()),
            channels: Some(vec![config.channel().to_string()]),
            ping_time: Some(10),
            ping_timeout: Some(10),
            ..Config::default()
        };

        let mut eb = ExponentialBackoff {
            current_interval: Duration::from_millis(1000),
            initial_interval: Duration::from_millis(1000),
            randomization_factor: 0.5,
            multiplier: 1.5,
            max_interval: Duration::from_millis(60000),
            max_elapsed_time: None, // Retry forever.
            clock: SystemClock::default(),
            start_time: Instant::now(),
        };
        eb.reset();

        IrcActor {
            addr,
            backoff: eb,
            config,
        }
    }

    fn start_stream(&mut self, ctx: &mut Context<Self>) -> Result<(), IrcError> {
        let client = IrcClient::from_config(self.config.clone())?;
        client.identify()?;
        ctx.add_stream(client.stream());

        info!("IRC stream started!");
        Ok(())
    }

    fn restart(&mut self, ctx: &mut Context<Self>) {
        if let Some(timeout) = self.backoff.next_backoff() {
            info!("Restarting with backoff: {:?}", timeout);
            ctx.run_later(timeout, |_act, ctx| ctx.stop());
        } else {
            info!("Stopping...");
            ctx.stop();
        }
    }
}

impl Actor for IrcActor {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) -> () {
        info!("IRC actor started!");

        match self.start_stream(ctx) {
            Ok(_) => info!("Stream started successfully."),
            Err(e) => {
                warn!("Error starting stream: {:?}", e);
                self.restart(ctx)
            }
        }
    }
}

impl Supervised for IrcActor {
    fn restarting(&mut self, _ctx: &mut Self::Context) {
        info!("Restarting!");
    }
}

impl StreamHandler<IrcMessage, IrcError> for IrcActor {
    fn handle(&mut self, item: IrcMessage, _ctx: &mut Self::Context) {
        if let Command::PONG(_, _) = item.command {
            self.backoff.reset();
            return;
        }

        self.addr.do_send(IrcPayload(item));
    }

    fn error(&mut self, err: IrcError, _ctx: &mut Self::Context) -> Running {
        warn!("IrcError: {:?}", err);
        Running::Stop
    }

    fn finished(&mut self, ctx: &mut Self::Context) {
        trace!("Stream finished.");
        self.restart(ctx)
    }
}
