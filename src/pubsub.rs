use actix::prelude::*;
use common::{
    AddSubscriber, IrcPayload, RemoveSubscriber, StructuredIrcMessage, SubscriptionId,
    SubscriptionResponse,
};
use std::collections::HashMap;
use std::time::Duration;
use web::SubscriptionSession;

pub struct MessageHandler {
    buffer: Vec<StructuredIrcMessage>,
    capacity: usize,
    subscribers: HashMap<SubscriptionId, Addr<Syn, SubscriptionSession>>,
}

impl MessageHandler {
    pub fn with_scrollback_capacity(capacity: usize) -> MessageHandler {
        MessageHandler {
            buffer: Vec::with_capacity(capacity),
            capacity,
            subscribers: HashMap::new(),
        }
    }

    fn print_subscriptions(&self, ctx: &mut <Self as Actor>::Context) {
        info!("{} subscriptions active.", self.subscribers.len());
        ctx.run_later(Duration::from_secs(60), |act, ctx| {
            act.print_subscriptions(ctx)
        });
    }

    fn publish(&mut self, message: StructuredIrcMessage) {
        if self.buffer.len() == self.capacity {
            // TODO: Some sort of circular buffer would be preferable.
            self.buffer.remove(0);
        }

        info!(
            "Publishing message to {} subscribers: {:?}",
            self.subscribers.len(),
            message
        );

        self.subscribers
            .values()
            .for_each(|addr| addr.do_send(message.clone()));

        self.buffer.push(message);
    }
}

impl Actor for MessageHandler {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.print_subscriptions(ctx)
    }
}

impl Handler<IrcPayload> for MessageHandler {
    type Result = ();

    fn handle(&mut self, msg: IrcPayload, _ctx: &mut Self::Context) {
        StructuredIrcMessage::from_message(msg.0).map(|msg| self.publish(msg));
    }
}

impl Handler<AddSubscriber> for MessageHandler {
    type Result = MessageResult<AddSubscriber>;

    fn handle(&mut self, msg: AddSubscriber, _ctx: &mut Self::Context) -> Self::Result {
        let subscription_id = SubscriptionId::random();

        self.subscribers.insert(subscription_id, msg.subscriber);
        info!(
            "Subscription added, {} subscriptions active.",
            self.subscribers.len()
        );

        MessageResult(SubscriptionResponse {
            subscription_id,
            scrollback: self.buffer.clone(),
        })
    }
}

impl Handler<RemoveSubscriber> for MessageHandler {
    type Result = ();

    fn handle(&mut self, msg: RemoveSubscriber, _ctx: &mut Self::Context) {
        self.subscribers.remove(&msg.subscription_id);
        info!(
            "Subscription removed, {} subscriptions active.",
            self.subscribers.len()
        );
    }
}
