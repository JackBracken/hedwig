use actix::{Addr, Message, Syn};
use chrono::{DateTime, Utc};
use irc::proto::Command;
use irc::proto::Message as IrcMessage;
use rand::prelude::thread_rng;
use rand::Rng;
use std::cmp;
use web::SubscriptionSession;

const ACTION_PREFIX: &str = "\x01ACTION ";

#[derive(StructOpt, Debug, Clone)]
#[structopt(name = "basic")]
pub struct HedwigConfig {
    #[structopt(short = "c", long = "channel", default_value = "#test")]
    channel: String,

    #[structopt(short = "l", long = "listen", default_value = "127.0.0.1:1255")]
    listen_addr: String,

    #[structopt(short = "n", long = "nickname", default_value = "HedwigDev")]
    nickname: String,

    #[structopt(short = "s", long = "server", default_value = "irc.darklordpotter.net")]
    server: String,
}

impl HedwigConfig {
    pub fn channel(&self) -> &String {
        &self.channel
    }

    pub fn listen_addr(&self) -> &String {
        &self.listen_addr
    }

    pub fn nickname(&self) -> &String {
        &self.nickname
    }

    pub fn server(&self) -> &String {
        &self.server
    }
}

#[derive(Clone, Debug, Message)]
pub struct IrcPayload(pub IrcMessage);

#[derive(Clone, Debug, Message, Serialize)]
#[serde(tag = "type")]
pub enum Event {
    ACTION { action: String },
    PRIVMSG { message: String },
    JOIN,
    PART { part_message: Option<String> },
    QUIT { quit_message: Option<String> },
}

#[derive(Clone, Debug, Message, Serialize)]
pub struct StructuredIrcMessage {
    pub username: String,
    pub event: Event,
    pub date: DateTime<Utc>,
}

impl StructuredIrcMessage {
    pub fn from_message(irc_message: IrcMessage) -> Option<StructuredIrcMessage> {
        let event: Event = match irc_message.command {
            Command::PRIVMSG(_, ref message) => {
                let match_len = cmp::min(ACTION_PREFIX.len(), message.len());
                if message.starts_with(ACTION_PREFIX) {
                    Event::ACTION {
                        action: message[match_len..message.len() - 1].to_string(),
                    }
                } else {
                    Event::PRIVMSG {
                        message: message.clone(),
                    }
                }
            }
            Command::JOIN(_, _, _) => Event::JOIN,
            Command::PART(_, ref part_message) => Event::PART {
                part_message: part_message.clone(),
            },
            Command::QUIT(ref quit_message) => Event::QUIT {
                quit_message: quit_message.clone(),
            },
            _ => return None,
        };

        Some(StructuredIrcMessage {
            username: irc_message.source_nickname().unwrap_or("").to_string(),
            event,
            date: Utc::now(),
        })
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Message, PartialEq)]
pub struct SubscriptionId(pub i64);

impl SubscriptionId {
    pub fn random() -> SubscriptionId {
        SubscriptionId(thread_rng().gen::<i64>())
    }
}

#[derive(Debug, Message)]
pub struct SubscriptionResponse {
    pub subscription_id: SubscriptionId,
    pub scrollback: Vec<StructuredIrcMessage>,
}

pub struct AddSubscriber {
    pub subscriber: Addr<Syn, SubscriptionSession>,
}

impl Message for AddSubscriber {
    type Result = SubscriptionResponse;
}

#[derive(Debug, Message)]
pub struct RemoveSubscriber {
    pub subscription_id: SubscriptionId,
}
