use actix::*;
use actix_web::{ws, Error, HttpRequest, HttpResponse};
use common::*;
use pubsub::MessageHandler;
use rand::{thread_rng, Rng};
use serde_json;
use std::time::Duration;
use std::time::Instant;

pub struct HedwigState {
    pub addr: Addr<Syn, MessageHandler>,
    pub config: HedwigConfig,
}

pub struct SubscriptionSession {
    pub addr: String,
    pub subscription_id: Option<SubscriptionId>,
    pub client_heartbeat: Instant,
    pub heartbeat: Instant,
}

impl SubscriptionSession {
    fn start_heartbeat(&self, ctx: &mut <Self as Actor>::Context) {
        ctx.run_later(Duration::from_secs(60), |act, ctx| {
            let heartbeat: i64 = thread_rng().gen::<i64>();
            ctx.ping(&heartbeat.to_string());
            act.start_heartbeat(ctx)
        });
    }
}

impl Actor for SubscriptionSession {
    type Context = ws::WebsocketContext<Self, HedwigState>;

    fn started(&mut self, ctx: &mut Self::Context) {
        info!("Starting subscription for {:?}", self.addr);

        let addr: Addr<Syn, _> = ctx.address();
        ctx.state()
            .addr
            .send(AddSubscriber { subscriber: addr })
            .into_actor(self)
            .then(|res: Result<SubscriptionResponse, _>, act, ctx| {
                match res {
                    Ok(res) => {
                        act.subscription_id = Some(res.subscription_id);

                        info!(
                            "Sending scrollback for {:?}, {} items",
                            res.subscription_id,
                            res.scrollback.len()
                        );

                        res.scrollback
                            .into_iter()
                            .map(|msg| serde_json::to_string(&msg).unwrap())
                            .for_each(|msg| ctx.text(msg));

                        act.start_heartbeat(ctx);
                    }
                    // something is wrong, abort.
                    _ => ctx.stop(),
                }
                fut::ok(())
            })
            .wait(ctx);
    }

    fn stopping(&mut self, ctx: &mut <Self as Actor>::Context) -> Running {
        if let Some(subscription_id) = self.subscription_id {
            ctx.state()
                .addr
                .do_send(RemoveSubscriber { subscription_id });
        }

        Running::Stop
    }
}

/// Handle messages from chat server, we simply send it to peer websocket
impl Handler<StructuredIrcMessage> for SubscriptionSession {
    type Result = ();

    fn handle(&mut self, msg: StructuredIrcMessage, ctx: &mut Self::Context) {
        ctx.text(serde_json::to_string(&msg).unwrap());
    }
}

impl StreamHandler<ws::Message, ws::ProtocolError> for SubscriptionSession {
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        debug!("WEBSOCKET MESSAGE: {:?}", msg);
        match msg {
            ws::Message::Ping(msg) => ctx.pong(&msg),
            ws::Message::Pong(_) => self.heartbeat = Instant::now(),
            ws::Message::Text(msg) => match msg.as_ref() {
                "ping" => self.client_heartbeat = Instant::now(),
                _ => {}
            },
            ws::Message::Binary(_) => {}
            ws::Message::Close(_) => {
                ctx.stop();
            }
        }
    }
}

pub fn chat_route(req: HttpRequest<HedwigState>) -> Result<HttpResponse, Error> {
    let addr = req
        .connection_info()
        .remote()
        .unwrap_or("<unknown>")
        .to_string();

    ws::start(
        req,
        SubscriptionSession {
            addr,
            subscription_id: None,
            client_heartbeat: Instant::now(),
            heartbeat: Instant::now(),
        },
    )
}
